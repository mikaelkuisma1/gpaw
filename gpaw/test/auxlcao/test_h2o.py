from gpaw import GPAW
from ase.build import molecule
from ase import Atoms

#atoms = molecule('H2O', vacuum=4)
atoms = Atoms('He')
atoms.center(vacuum=4)
calc = GPAW(mode='lcao', xc='PBE0:backend=aux-lcao')
atoms.set_calculator(calc)

atoms.get_potential_energy()

