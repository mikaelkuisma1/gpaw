from gpaw.atom.radialgd import EquidistantRadialGridDescriptor

class AuxiliaryBasisFunctionSet:
    def __init__(self, rgd, l_x = [], auxt_xg = [], wauxt_xg = []):
        self.rgd = rgd
        self.auxt_xg = auxt_xg
        self.wauxt_xg = wauxt_xg
        self.l_x = l_x

    def append(self, l, auxt_xg):
        wauxt_xg = self.rgd.poisson(auxt_xg, l)
        self.l_x.append(l)
        self.auxt_xg.append(auxt_xg)
        self.wauxt_xg.append(wauxt_xg)       

def create_big_auxbasis():
    rgd = EquidistantRadialGridDescriptor(h=0.05, N=1000)
    auxbasis = AuxiliaryBasisFunctionSet(rgd)
    for l in range(2):
        for alpha in np.logspace(-1,1, 5):
            auxbasis.append(l, exp(-alpha*rgd.r_g**2))

class AuxiliaryBasisFunctions:
    def __init__(self, gd, symbols_a, aux_params):
        self.gd = gd
        self.symbols_a = symbols_a
        self.aux_params = aux_params

        self.auxbasis_a = {}
        for a, symbol in enumerate(symbols_a):
            auxbasistype = aux_params[symbol]
            if auxbasistype == 'Big':
                auxbasis = create_big_auxbasis()
            else:
                raise ValueError('Unknown aux-basis.')
            print('Atom %d %s: aux basis %s' % (a, symbol, aux_params[symbol]))
            self.auxbasis_a[a] = auxbasis

    def set_positions(self, spos_ac):
        print('Call to AuixiliaryBasisFunctions.set_positions')

