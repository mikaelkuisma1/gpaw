import numpy as np

from gpaw.xc import XC
from math import pi
from gpaw.gaunt import gaunt
from gpaw.lfc import LFC

from gpaw.lcao.tci import AUXTCIExpansions

from gpaw.utilities import (pack_atomic_matrices, unpack_atomic_matrices,
                            unpack2, unpack, packed_index, pack, pack2)
G_LLL = gaunt(3) # XXX

from gpaw.utilities.tools import tri2full

from gpaw.auxlcao.multipole import mpW_LL

from gpaw.auxlcao.matrix_elements import MatrixElements

cutoff = 11.0

"""
The methods are called in following order
   set_aux_basis
   set_grid_descriptor
   initialize
   set positions
"""

class LCAOHybrid:
    orbital_dependent = True
    type = 'HYB'

    def __init__(self, xcname: str, **kwargs):
        self.matrix_elements = MatrixElements()
        self.debug_hybrid = True
        if xcname == 'PBE0':
            self.exx_fraction = 0.25
            self.name = xcname
            self.xc = XC('HYB_GGA_XC_PBEH')
        elif xcname == 'EXX':
            self.exx_fraction = 1.0
            self.name = xcname
            self.xc = XC('null')
        elif xcname == 'Hartree':
            self.exx_fraction = 0.0
            self.name = xcname
            self.xc = XC('null')
        else:
            raise ValueError("functional %s not supported by aux-lcao backend" % xcname)

        self.description = 'Work in progress RI-K auxiliriary basis set implementation of %s' % xcname
        #print(**kwargs)
        self.evv = 0.0
        self.ecc = 0.0
        self.evc = 0.0
        self.ekin = 0.0
        self.reference_hybrid = None
        self.gd = None

        if self.debug_hybrid:
            self.aux_lfc = None
            self.reference_hybrid = None

    def set_aux_basis(self, aux_params):
        self.matrix_elements.set_parameters(aux_params)

    def get_extra_setup_data(self, xxx):
        pass

    def set_positions(self, spos_ac):
        self.matrix_elements.set_positions_and_cell(spos_ac, self.ham.gd.cell_cv, self.ham.gd.pbc_c)

        #print('Call to set positions')

        # Calculate generalized Gaussian - generalized Gaussian matrix elements
        self.tci = self.tciexpansions.get_tci_calculator(self.ham.gd.cell_cv, spos_ac, self.ham.gd.pbc_c, self.wfs.kd.ibzk_qc, self.wfs.dtype)

        if self.debug_hybrid:
            np.set_printoptions(formatter={'float': lambda x: "{0:0.6f}".format(x)})

            finegd = self.density.finegd
            self.aux_lfc.set_positions(spos_ac)

            # Count the number of auxiliary functions
            Atot = 0
            for a, setup in enumerate(self.wfs.setups):
                for j, auxt in enumerate(setup.auxt_j):
                    for m in range(2*auxt.l+1):
                        Atot += 1

            nao = self.wfs.setups.nao

            self.W_AA = np.zeros( (Atot, Atot) )
            self.I_AMM = np.zeros( (Atot, nao, nao) )
            self.G_AaL = []

            Aind = 0
            for a, setup in enumerate(self.wfs.setups):
                #print('Atom %d' % a)
                M = 0
                for j, aux in enumerate(setup.auxt_j):
                    print('   aux %d' % j)
                    for m in range(2*aux.l+1):
                        print('       m=%d' % (m-aux.l))
                        Q_aM = self.aux_lfc.dict(zero=True)
                        Q_aM[a][M] = 1.0

                        auxt_g = self.aux_lfc.gd.zeros()
                        wauxt_g = self.aux_lfc.gd.zeros()
                        self.aux_lfc.add(auxt_g, Q_aM)
                        self.ham.poisson.solve(wauxt_g, auxt_g, charge=None)

                        G_aL = self.density.ghat.dict(zero=True)
                        self.density.ghat.integrate(wauxt_g, G_aL)
                        self.G_AaL.append(G_aL)

                        # Calculate W_AA
                        W_aM = self.aux_lfc.dict(zero=True)
                        self.aux_lfc.integrate(wauxt_g, W_aM)

                        Astart = 0
                        for a2 in range(len(self.wfs.setups)):
                            W_M = W_aM[a2]
                            self.W_AA[Aind, Astart:Astart+len(W_M)] = W_M
                            Astart += len(W_M)

                        # Calculate W_Imm
                        wauxt_G = self.ham.gd.zeros()
                        self.ham.restrict(wauxt_g, wauxt_G)
                        V_MM = self.wfs.basis_functions.calculate_potential_matrices(wauxt_G)[0]
                        tri2full(V_MM)
                        self.I_AMM[Aind] = V_MM

                        M += 1
                        Aind += 1
            self.W_AA = (self.W_AA + self.W_AA.T)/2
            #for e in np.linalg.eigvalsh(self.W_AA):
            #    print("W_AA eig", e)

            self.fast_W_AA = np.zeros_like(self.W_AA)
            cols = []
            for a1, setup in enumerate(self.wfs.setups):
                rows = []
                for a2, setup in enumerate(self.wfs.setups):
                    rows.append(self.tci.W_AA(a1, a2)[0]) #XXX
                cols.append(np.hstack(tuple(rows)))
            self.fast_W_AA = np.vstack(tuple(cols))
            print('Reference W_AA', self.W_AA)
            print('Fast W_AA', self.fast_W_AA)
            print('Fast W_AA / reference W_AA', self.fast_W_AA / self.W_AA)

            R_c = np.dot(spos_ac[0] - spos_ac[1], self.ham.gd.cell_cv)
            print("Multipole W_AA", mpW_LL(R_c))
            print("Reference W_AA", self.W_AA[:9,9:])
            print("Multipole W_AA / reference W_AA", mpW_LL(R_c) / self.W_AA[:9,9:])

            xxx
            #print("W_AA", self.W_AA)
            #print("I_AMM", self.I_AMM)


            self.fast_I_AMM = np.zeros_like(self.I_AMM)
           
            print("Reference I_AMM", self.I_AMM)
            for a1, setup1 in enumerate(self.wfs.setups):
                M2start = 0
                A1start_atom = 0
                for a2, setup2 in enumerate(self.wfs.setups):

                    # 1. We want to evaluate a 'pseudo' three center integral, where two of
                    # the centers are the same. A and M1 are in the same center.
                    #
                    #           /       a1      a1      a2
                    # I_AM M  = | dr   W (r) phi (r) phi (r)                         (1)
                    #     1 2   /       A       M1      M2

                    # We may expand the same center product using Gaunt coefficients
                    #    a1      a1     --               a1        a1
                    #   W (r) phi (r) = \   G(L ,L  ,L) W (|r|) phi (|r|) Y (r)       (2)
                    #    A       M1     /_L    A  M1     j_A       j_M1    L
                    #
                    # So, that Eq. (1) becomes
                     
                    #         
                    #   /    --               a1        a1               a2
                    #   | dr \   G_L  L   L  W (|r|) phi (|r|) Y_L(r) phi (|r|) Y_{L_M2}(r)  =     (3)
                    #   /    /_L    A  M1     j_A       j_M1             j_M2

                    # Moving integral forward        
                    #    --             /     a1        a1               a2
                    #    \   G_L  L   L | dr W (|r|) phi (|r|) Y_L(r) phi (|r|) Y_{L_M2}(r)        (4)
                    #    /_L    A  M1   /     j_a       j_M1             j_M2

                    # Let
                    #        /       a1        a1               a2        
                    # W_XM = | dr   W (|r|) phi (|r|) Y_L(r) phi (|r|) Y_{L_M2}(r)
                    #        /       j_a       j_M1             j_M2
                    #
                    
                    #            --              
                    # I_AM1M2 =  \   G_L  L   L  W_X(A, M1, L)M2                     (6)
                    #            /_L    A  M1    
                    #
 
                    # Here, we evaluate the W_XM matrix, i.e. Eq. 5
                    W_XM = self.tci.W_XM(a1, a2)
                    assert W_XM.shape[0] == 1
                    W_XM = W_XM[0] # XXX What is this, q?
                    M2len = W_XM.shape[1]

                    # The lhs of Eq. 6 has 3 items, here we loop over those
                    
                    # 1) Loop over A
                    A = 0
                    X = 0
                    Astart = Astart_atom
                    for j, auxt in enumerate(setup1.auxt_j):
                        M1start = 0
                        for j1, phit1 in enumerate(setup1.phit_j):                           
                            for l in range((auxt.l + phit1.l) % 2, auxt.l + phit1.l + 1, 2):
                                for m in range(2*l+1):
                                    LX = l**2 + m
                                    for m1 in range(2*phit1.l+1):
                                        M1 = M1start + m1
                                        L1 = phit1.l**2 + m1
                                        for mA in range(2*auxt.l+1):
                                            LA = auxt.l**2 + mA
                                            A = Astart + mA
                                            #print(self.fast_I_AMM[A, M1, :],"1")
                                            #print(G_LLL[LA,L1,LX],"2")
                                            #print(W_XM[X, :],"3")
                                            #for M2 in range(W_XM.shape[1]):
                                            #    print(A,M1,M2,LA,L1,LX,X,M2)
                                            #    print(self.fast_I_AMM.shape)
                                            #    print(G_LLL.shape)
                                            #    print(W_XM.shape)
                                            #    self.fast_I_AMM[A, M1, M2] += G_LLL[LA,L1,LX] * W_XM[X, M2]
                                            # XXX With : instead of M2 for loop, one gets, why???
                                            print("LA,L1,LX=", LA, L1, LX, G_LLL[LA,L1,LX], "W_XM", X, W_XM[X, :], " to ", A, M1, ":")
                                            self.fast_I_AMM[A, M1, M2start:M2start+M2len] += G_LLL[LA,L1,LX] * W_XM[X, :]
                                            # ValueError: non-broadcastable output operand with shape (1,) doesn't match the broadcast shape (1,1)
                                    X += 1

                            M1start += 2*phit1.l+1
                        Astart += 2*auxt.l+1
                    M2start += M2len


            for a,b in zip(self.I_AMM.ravel(), self.fast_I_AMM.ravel()):
                print(a,b,a-b, a/b)
            print(W_XM)
            xxx

            # I_AMM += \sum_L \sum_a \sum_ii Delta^a_Lii P_aMi P_aMi G_AaL

            for A in range(Atot):
                for a, setup in enumerate(self.wfs.setups):
                    #print('Atomic corrections for atom %d to aux %d.' % (a, A))
                    #setup.Delta_iiL
                    G_L = self.G_AaL[A][a]
                    X_ii = np.dot(setup.Delta_iiL, G_L)
                    P_Mi = self.wfs.atomic_correction.P_aqMi[a][0]
                    self.I_AMM[A] += P_Mi @  X_ii @ P_Mi.T           

            self.iW_AA = np.linalg.inv(self.W_AA)

        #self.auxiliary_basisfunctions.set_positions(spos_ac)
    
    def get_setup_name(self):
        return 'PBE'

    def write(self, writer):
        pass

    def read(self, reader):
        pass

    def set_grid_descriptor(self, gd):
        self.gd = gd
        #print('Call to set_grid_descriptor')

    def calculate(self, gd, nt_sr, vt_sr):
        energy = self.ecc + self.evv + self.evc
        energy += self.xc.calculate(gd, nt_sr, vt_sr)
        #print("energy calculate", energy)
        return energy

    def get_description(self):
        return self.description

    def add_nlxc_matrix(self, H_MM, dH_asp, wfs, kpt):
        self.evv = 0.0
        self.evc = 0.0
        self.ekin = 0.0

        if kpt.C_nM is None:
            #print("kpt.C_nM is None. Not calculating Fock Matrix.")
            return 

        rho_MM = wfs.ksl.calculate_density_matrix(kpt.f_n, kpt.C_nM)

        for a3,a4 in self.matrix_elements.a1a2_p:
            # 1. Pick the block from rho_MM corresponding to a3,a4
            rhoa3a4_MM = rho_MM[a3,a4]

            for a1 in orbitals_overlapping_wit_a3:
                Pa1a3_AMM = I[a1,a3]._AMM @ rhoa3a4_MM

                for a2 in orbitals_overlapping_with_a4[a4]:
                    W_AA[a4, a1] @ Pa1a3_AMM

            # Loop over all overlapping pairs of a1
            fo

            # 2. Calculate projection matrix
            projection_AMM = einsum('Aij,jk', I_AMM, rho_MM)
            
            # Coulomb
            for a3 in atoms:
                store(a3, W_AA @ projection_AMM)

            # Big multiply in auxiliary basis
            W_bigA @ projection_AMM
            W_bigAMM
        #print("Enter add_nlxc_matrix")
        # XXX Do not recalculate

        

        #if self.reference_hybrid is None:
        #    from gpaw.auxlcao.debugtools import ReferenceCoulombMatrix
        #    self.reference_hybrid = ReferenceCoulombMatrix(self.ham, wfs, self.density, self.exx_fraction)

        F_MM = -0.5 * np.einsum('Aij,AB,Bkl,jl', self.I_AMM, self.iW_AA, self.I_AMM, rho_MM, optimize=True) 
        
        H_MM += self.exx_fraction * F_MM
        #print("NOT USING RIGHT FOCK MATRIX")

        self.evv = 0.5 * self.exx_fraction * np.einsum('ij,ij', F_MM, rho_MM)

        #F2_MM = np.zeros_like(F_MM)
        #exc_ref = self.reference_hybrid.add_nlxc_matrix(F2_MM, rho_MM, kpt)
        #H_MM += self.exx_fraction * F2_MM
        #print("F_MM - F2_MM", F_MM - F2_MM)
        
        for a in dH_asp.keys():
            #print(a)
            D_ii = unpack2(self.density.D_asp[a][0]) / 2 # Check 1 or 2
            # Copy-pasted from hybrids/pw.py
            ni = len(D_ii)
            V_ii = np.empty((ni, ni))
            for i1 in range(ni):
                for i2 in range(ni):
                    V = 0.0
                    for i3 in range(ni):
                        p13 = packed_index(i1, i3, ni)
                        for i4 in range(ni):
                            p24 = packed_index(i2, i4, ni)
                            V += self.density.setups[a].M_pp[p13, p24] * D_ii[i3, i4]
                    V_ii[i1, i2] = +V
            V_p = pack2(V_ii)
            dH_asp[a][0] += (-V_p - self.density.setups[a].X_p) * self.exx_fraction

            #print("Atomic Ex correction", np.dot(V_p, self.density.D_asp[a][0]) / 2)
            #print("Atomic Ex correction", np.trace(V_ii @ D_ii))
            self.evv -= self.exx_fraction * np.dot(V_p, self.density.D_asp[a][0]) / 2
            self.evc -= self.exx_fraction * np.dot(self.density.D_asp[a][0], self.density.setups[a].X_p)
        
        self.ekin = -2*self.evv -self.evc
        #print("evv", self.evv, " Hartree")#, "eref", exc_ref)
        #print("Exit add_nlxc_matrix")

    def initialize(self, density, ham, wfs):
        #print("Call to initialize")
        self.matrix_elements.initialize(density, ham, wfs)
        self.ham = ham
        self.density = density
        self.wfs = wfs
        self.ecc = sum(setup.ExxC for setup in wfs.setups) * self.exx_fraction
        #self.symbols_a = atoms.get_chemical_symbols()
        assert wfs.world.size == wfs.gd.comm.size

        # Copy pasted from setup.py
        def H(rgd, n_g, l):
            v_g = rgd.poisson(n_g, l)
            v_g[1:] /= rgd.r_g[1:]
            v_g[0] = v_g[1]
            return v_g

        if self.debug_hybrid:
            # For each atom
            auxt_aj = []
            for a, sphere in enumerate(wfs.basis_functions.sphere_a):
                auxt_j = []
                wauxt_j = []
                rgd = wfs.setups[a].rgd

                """                
                # This code naiively just creates an auxiliary basis function for each basis function pair
                # of the atom:
                #
                # \tilde \Theta_A(r) = \tilde \phi_i(r) \tilde \phi_j(r),
                #
                # Create auxiliary wave function space naiively just with
                for j1, spline1 in enumerate(sphere.spline_j):
                    for j2, spline2 in enumerate(sphere.spline_j):
                        if j1 > j2:
                            continue
                        f_g = rgd.zeros()
                        l1 = spline1.get_angular_momentum_number()
                        l2 = spline2.get_angular_momentum_number()
                        for g, r in enumerate(rgd.r_g):
                            f_g[g] = spline1(r) * spline2(r) * r ** (l1+l2)
                        for l in range((l1 + l2) % 2, l1 + l2 + 1, 2):
                            if l > 2:
                                continue
                            auxt_j.append(rgd.spline(f_g, cutoff, l, 100))
                            wauxt_j.append(rgd.spline(H(rgd, f_g, l), cutoff, l, 100))
                """
                print('Only adding the compensation charges')
               
                #ghat_l = []
                wghat_l = []
                for l in range(3):
                    spline = wfs.setups[a].ghat_l[l]
                    f_g = rgd.zeros()
                    for g, r in enumerate(rgd.r_g):
                        f_g[g] = spline(r) * r**l

                    # XXX Debug code no longer works, because ghat's are not aded to auxt_j
                    #ghat_l.append(wfs.setups[a].ghat_l[l])
                    #wghat_l.append(rgd.spline(H(rgd, f_g, l), cutoff, l, 100)) # XXX
                    
                    auxt_j.append(wfs.setups[a].ghat_l[l])
                    wauxt_j.append(rgd.spline(H(rgd, f_g, l), cutoff, l,100)) # XXX
                

                auxt_aj.append(auxt_j)

                wfs.setups[a].auxt_j = auxt_j
                wfs.setups[a].wauxt_j = wauxt_j
                #wfs.setups[a].ghat_l = ghat_l
                wfs.setups[a].wghat_l = wghat_l

                x = 0
                wauxtphit_x = []
                for wauxt in wauxt_j:
                    for j1, spline1 in enumerate(sphere.spline_j):
                        f_g = rgd.zeros()
                        l1 = wauxt.get_angular_momentum_number()
                        l2 = spline1.get_angular_momentum_number()
                        for g, r in enumerate(rgd.r_g):
                            f_g[g] = spline1(r) * wauxt(r) * r ** (l1+l2)
                        for l in range((l1 + l2) % 2, l1 + l2 + 1, 2):
                            wauxtphit_x.append(rgd.spline(f_g, cutoff, l))
                wfs.setups[a].wauxtphit_x = wauxtphit_x

            self.aux_lfc = LFC(density.finegd, auxt_aj)

        self.timer = ham.timer
        with self.timer('AUXTCI: Evaluate splines'):
            self.tciexpansions = AUXTCIExpansions.new_from_setups(wfs.setups)

    def calculate_paw_correction(self, setup, D_sp, dH_sp=None, a=None):
        return self.xc.calculate_paw_correction(setup, D_sp, dH_sp, a=a)

    def get_kinetic_energy_correction(self):
        return self.ekin

    def summary(self, log):
        log(self.description)



"""
XXXXXXXXXXXXXXXXXX
        if 0:  # self.debug_tci:
            # if self.ksl.using_blacs:
            #     self.tci.set_matrix_distribution(Mstart, mynao)
            oldS_qMM = np.empty((nq, mynao, nao), self.dtype)
            oldT_qMM = np.empty((nq, mynao, nao), self.dtype)

            oldP_aqMi = {}
            for a in self.basis_functions.my_atom_indices:
                ni = self.setups[a].ni
                oldP_aqMi[a] = np.empty((nq, nao, ni), self.dtype)

            # Calculate lower triangle of S and T matrices:
            self.timer.start('tci calculate')
            # self.tci.calculate(spos_ac, oldS_qMM, oldT_qMM,
            #                   oldP_aqMi)
            self.timer.stop('tci calculate')

XXXXXXXXXXXXXXXXXX
"""



