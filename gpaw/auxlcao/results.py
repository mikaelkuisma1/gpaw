from gpaw import GPAW
from ase.build import molecule
from ase import Atoms
from sys import argv
from os.path import exists
import numpy as np
from ase.collections import g2

names = g2.names
names.append('He')
names.append('Ne')
for mol in names:
    for xc in ['PBE','PBE0','EXX']:
        for mode in ['lcao','pw']:
            prefix = '%s_%s_%s' % (mode, xc, mol)
            if not exists('%s.gpw' % prefix):
                continue
            calc = GPAW('%s.gpw' % prefix)
            if mode == 'lcao':
                lcao_E = calc.get_potential_energy()
                lcao_eigs = calc.get_eigenvalues()
                #print('energy', lcao_E)
                #print('eigs', lcao_eigs)
            else:
                pw_E = calc.get_potential_energy()
                pw_eigs = calc.get_eigenvalues()
                pw_occs = calc.get_occupation_numbers()
                #print('energy', pw_E)
                #print('eigs', pw_eigs)

                print('%s %s lcao vs. pw energy diff %+.15g' % (mol, xc, lcao_E-pw_E))
                for i in range(len(pw_eigs)):
                    try:
                        print('%s %s lcao vs. pw eig diff %03d %.2f %+.15g' % (mol, xc, i, pw_occs[i], lcao_eigs[i]-pw_eigs[i]))
                    except Exception as e:
                        print('error %s' % str(e))

                lcao_E = np.inf
                lcao_eigs = np.inf



