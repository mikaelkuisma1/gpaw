

"""
The Fock Operator is given as


  Fij = K_ikjl rho_kl          (1)

  K_ikjl requires overlap from jl and ik.
  
  Let p1 = (i,k) and p2 = (j,l).

  We need to loop only over overlapping pairs.
 
  To refer to components, we use for example i_p1, k_p1.


  Thus, Eq. (1) may be written as

  F            = K                        rho
   i_p1, j_p2     i_p1, k_p1, j_p2, l_p2     k_p1, l_p2



  We may expand K as
                                                                         -1                  -1
  K                       = \sum             ( i_p1, k_p1 || A1 ) (A1||A2)   (A2||A3) (A3||A4) (A4|| j_p2, l_p2)
   i_p1, k_p1, j_p2, l_p2       A1,A2,A3,A4


If all all matrices are defined in full auxiliary basis, the one inverse cancels one auxiliary Coulomb matrix, and one
is left with a single inverse auxilary Coulomb matrix.

However, the problem quickly becomes ill-defined.

"""


from ase.neighborlist import PrimitiveNeighborList


class AtomPairRegistry:
    def __init__(self, cutoff_a, pbc_c, cell_cv, spos_ac):
        nl = PrimitiveNeighborList(cutoff_a, skin=0, sorted=True,
                                   self_interaction=True,
                                   use_scaled_positions=True)

        nl.update(pbc=pbc_c, cell=cell_cv, coordinates=spos_ac)
        r_and_offset_aao = {}

        def add(a1, a2, R_c, offset):
            r_and_offset_aao.setdefault((a1, a2), []).append((R_c, offset))

        for a1, spos1_c in enumerate(spos_ac):
            a2_a, offsets = nl.get_neighbors(a1)
            for a2, offset in zip(a2_a, offsets):
                spos2_c = spos_ac[a2] + offset

                R_c = np.dot(spos2_c - spos1_c, cell_cv)
                add(a1, a2, R_c, offset)
                if a1 != a2 or offset.any():
                    add(a2, a1, -R_c, -offset)
        self.r_and_offset_aao = r_and_offset_aao

    def get(self, a1, a2):
        R_c_and_offset_a = self.r_and_offset_aao.get((a1, a2))
        return R_c_and_offset_a

    def get_atompairs(self):
        return list(sorted(self.r_and_offset_aao))

