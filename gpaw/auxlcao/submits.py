from ase.collections import g2
import numpy as np
from ase.build import molecule

for mol in g2.names:
    atoms = molecule(mol)
    if np.any(atoms.get_initial_magnetic_moments()):
        continue

    f = open('submit_%s.job' % mol,'w')
    print("""#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kuisma@dtu.dk
#SBATCH --partition=xeon16
#SBATCH -n 1
#SBATCH --time=1-02:00:00
#SBATCH --mem-per-cpu=16GB
#SBATCH --output=submit_%s.out
#SBATCH --error=submit_%s.err

python mytest.py %s

""" % (mol, mol, mol), file=f)
    f.close()
    print('sbatch submit_%s.job' % mol)
    print('sleep 60')
