from gpaw import GPAW
from ase.build import molecule
from ase import Atoms
from sys import argv
from os.path import exists

from ase.collections import g2

from ase.db import connect
db = connect('reference.db')

for mol in argv[1:]:
    for xc in ['PBE0','PBE','EXX']:
        for mode in ['lcao', 'pw']:
            if mol == 'He' or mol == 'Ne':
                atoms = Atoms(mol)
            else:
                atoms = molecule(mol)
            atoms.center(vacuum=5)

            prefix = '%s_%s_%s' % (mode, xc, mol)
            if xc == 'PBE':
                xc_string = 'PBE'
            else:
                if mode == 'lcao':
                    xc_string = xc + ':backend=aux-lcao'
                else:
                    xc_string = xc + ':backend=pw'

            if exists('%s.gpw' % prefix):
                continue

            calc = GPAW(h=0.17, mode=mode, xc=xc_string, basis='dzp', txt='%s.out' % prefix)
            atoms.set_calculator(calc)
            atoms.get_potential_energy()
            calc.write('%s.gpw' % prefix)
