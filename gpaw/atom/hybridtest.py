from gpaw.atom.aeatom import RIAllElectronAtom
import numpy as np

def check(kind, val, ref):
    if ref is None:
       print('%20s %+13.9f' % (kind, val))
    else:
        logdiff = np.log10(np.abs(val-ref))
        print('%20s %+13.9f %+13.9f %+13.9f %+5.2f' % (kind, val, ref, val-ref, logdiff))
        #assert(logdiff <-4)

def Mg(xc, aux='sdp'):
    s =  [  [ (4953.8339,    -0.57778967E-02),
              (745.18044,    -0.43124761E-01),
              (169.21605,    -0.19268217),
              (47.300672,    -0.48641439),
              (14.461337,    -0.42550894) ],
            [ (24.768175,    0.87956970E-01),
              (2.4940945,    -0.55165058),
              (0.87807585,   -0.53443295) ],
            [ (0.87212782E-01, 1.0000000) ],
            [ (0.33599294E-01,  1.0000000) ] ]
    p = [  [ (98.053010,    -0.14480565E-01),
            (22.586932,    -0.95495751E-01),
            (6.8391510,    -0.30787673),
            (2.2332844,    -0.49936293),
            (0.71606599,    -0.31503476) ],
          [ (0.18914796,      1.0000000) ] ]
    basis_l = [s, p]

    if aux == 'sdp':
        auxs = [ [ (2128.2496,     0.10643319),
                   (807.93042,     0.16789291),
                   (323.75323,     0.47543677),
                   (136.77209,     0.69573362) ],
                 [ (60.815704,     0.50039840) ],
                 [ (28.404043,     1.0000000) ],
                 [ (13.899707,     1.0000000) ],
                 [ (7.1055414,     1.0000000) ],
                 [ (3.7812917,      1.0000000) ],
                 [ (2.0863957,      1.0000000) ],
                 [ (1.1882511,      1.0000000) ],
                 [ (0.69503590,      1.0000000) ],
                 [ (0.41527650,      1.0000000) ],
                 [ (0.25199010,      1.0000000) ],
                 [ (0.15435110,      1.0000000) ],
                 [ (0.94842800E-01,  1.0000000) ] ]

        auxp = [ [ (396.48153,     0.98933060E-01),
                   (152.10945,     0.29527731),
                   (61.142601,     0.71625501),
                   (25.673860,     0.62450165) ],
                 [ (11.222348,      1.0000000) ],
                 [ (5.0860890,      1.0000000) ],
                 [ (2.3792166,      1.0000000) ],
                 [ (1.1430544,      1.0000000) ],
                 [ (0.56095400,      1.0000000) ],
                 [ (0.27957660,      1.0000000) ],
                 [ (0.14065420,      1.0000000) ],
                 [ (0.70986000E-01,  1.0000000) ] ]
    
        auxd = [ [ (101.57104,     0.86280838E-01),
                   (42.395910,     0.31056467),
                   (18.064562,     0.94662833) ],
                 [ (7.8352029,     1.0000000) ],
                 [ (3.4489951,     1.0000000) ],
                 [ (1.5360109,     1.0000000) ],
                 [ (0.68983670,    1.0000000) ],
                 [ (0.31138900,    1.0000000) ],
                 [ (0.14079840,    1.0000000) ] ]

        aux_l = [ auxs, auxp, auxd ]
    elif aux == 'acc':
        aux_l = []
        for l in range(3):
            auxbasis = []
            for alpha in np.logspace(-2, 4, 33):
                auxbasis.append( [ (alpha, 1.0) ] )
            aux_l.append(auxbasis)
    else:
        raise ValueError('Unkown basis')


    if xc == 'HF':
        aea = RIAllElectronAtom('Mg', xc='null', spinpol=False, dirac=False) # XXX xc is not used!
        aea.initialize(ngpts=60000, alpha2=150000000,rcut=70.0, 
                       override_basis_l = basis_l, auxiliary_basis_l = aux_l,
                       hybrid_mix=1.0)
        aea.run()

        check("Total energy", aea.etot, -199.53471767428)
        check("1s eig",       aea.channels[0].e_n[0], -49.03871)
        check("2s eig",       aea.channels[0].e_n[1], -3.77672)
        check("3s eig",       aea.channels[0].e_n[2], -0.25160)
        check("2p eig",       aea.channels[1].e_n[0], -2.29205)
        check("p* eig",       aea.channels[1].e_n[1], 0.24835)
        check("Exc (nl)",     aea.nlexc,             -15.9672414555)
        check("Exc (local)",  aea.exc,                0.0)
        check("EH",           aea.eH,                 95.6155919906)
    elif xc == 'PBE':
        aea = RIAllElectronAtom('Mg', xc='PBE', spinpol=False, dirac=False) # XXX xc is not used!
        aea.initialize(ngpts=60000, alpha2=150000000,rcut=70.0, 
                       override_basis_l = basis_l, auxiliary_basis_l = aux_l,
                       hybrid_mix=0.0)
        aea.run()
    elif xc == 'LDA':
        aea = RIAllElectronAtom('Mg', xc='LDA', spinpol=False, dirac=False) # XXX xc is not used!
        aea.initialize(ngpts=60000, alpha2=150000000,rcut=70.0, 
                       override_basis_l = basis_l, auxiliary_basis_l = aux_l,
                       hybrid_mix=0.0)
        aea.run()

        check("Total energy", aea.etot,               None)
        check("1s eig",       aea.channels[0].e_n[0], None)
        check("2s eig",       aea.channels[0].e_n[1], None)
        check("3s eig",       aea.channels[0].e_n[2], None)
        check("2p eig",       aea.channels[1].e_n[0], None)
        check("p* eig",       aea.channels[1].e_n[1], None)
        check("Exc",          aea.exc,                None)
        check("EH",           aea.eH,                 None)
    #elif xc == 'HSE06':
    #    xc = 'HYB_GGA_XC_HSE06'
    #    aea = RIAllElectronAtom('Mg', xc=xc, spinpol=False, dirac=False) # XXX xc is not used!
    #    aea.initialize(ngpts=60000, alpha2=150000000,rcut=70.0, 
    #                   override_basis_l = basis_l, auxiliary_basis_l = aux_l,
    #                   hybrid_mix=0.25, exchange_gamma=0.11)
    #    aea.run()
    #
    #    check("Total energy", aea.etot,               -199.88904860446)
    #    check("1s eig",       aea.channels[0].e_n[0], -46.91873)
    #    check("2s eig",       aea.channels[0].e_n[1], -3.13899)
    #    check("3s eig",       aea.channels[0].e_n[2],-0.18472)
    #    check("2p eig",       aea.channels[1].e_n[0],-1.86208)
    #    check("p* eig",       aea.channels[1].e_n[1], 0.09593)
    #    check("Exc",          aea.exc,                None)
    #    check("EH",           aea.eH,                 None)
    elif xc == 'PBE0':
        xc = 'HYB_GGA_XC_PBEH'
        aea = RIAllElectronAtom('Mg', xc=xc, spinpol=False, dirac=False) # XXX xc is not used!
        aea.initialize(ngpts=60000, alpha2=150000000,rcut=70.0, 
                       override_basis_l = basis_l, auxiliary_basis_l = aux_l,
                       hybrid_mix=0.25)
        aea.run()
        check("Total energy", aea.etot,               -199.88965889649)
        check("1s eig",       aea.channels[0].e_n[0], -46.93429)
        check("2s eig",       aea.channels[0].e_n[1], -3.15470)
        check("3s eig",       aea.channels[0].e_n[2], -0.19888)
        check("2p eig",       aea.channels[1].e_n[0], -1.87765)
        check("p* eig",       aea.channels[1].e_n[1], 0.11035)
        check("Exc",          aea.exc,                None)
        check("EH",           aea.eH,                 None)
    else:
        raise ValueError('Unknown benchmark')


def Be():
    s = []
    for alpha in np.logspace(-2, 2, 7):
        s.append( [ (alpha, 1.0) ] )
    basis_l = [s ]

    auxs = []
    for alpha in np.logspace(-3, 3, 17):
       auxs.append( [ (alpha, 1.0) ] )

    aux_l = [ auxs ]
    aea = RIAllElectronAtom('Be', xc='PBE', spinpol=False, dirac=False)
    aea.initialize(ngpts=30000, alpha2=6000000,rcut=50.0, override_basis_l = basis_l, auxiliary_basis_l = aux_l)
    #aea.initialize(override_basis_l = basis_l)
    aea.run()


def He():
    s = []
    for alpha in np.logspace(-2, 2, 7):
        s.append( [ (alpha, 1.0) ] )
    basis_l = [s ]

    auxs = []
    for alpha in np.logspace(-3, 3, 17):
       auxs.append( [ (alpha, 1.0) ] )

    aux_l = [ auxs ]
    xc = 'PBE0'
    aea = RIAllElectronAtom('He', xc=xc, spinpol=False, dirac=False)
    aea.initialize(ngpts=30000, alpha2=6000000,rcut=50.0, override_basis_l = basis_l, auxiliary_basis_l = aux_l, hybrid_mix=0.25, exchange_gamma=0.11)

    #aea.initialize(override_basis_l = basis_l)
    aea.run()

#Mg('PBE', aux='acc')
#Mg('PBE0', aux='acc')
He()
#for xc in ['LDA', 'HF','PBE0']:
#    Mg(xc)



""" Turbomole RI-JK-HF vs. GPAW


Solving non-relativistic Schrödinger equation using Gaussian basis-set:
.....................
Converged in 20 steps

 state  occupation         eigenvalue          <r>
 nl                  [Hartree]        [eV]    [Bohr]
-----------------------------------------------------
 1s        2.000    -49.038706    -1334.41115  0.131
 2s        2.000     -3.776721     -102.76981  0.692
 2p        6.000     -2.292052      -62.36990  0.688
 3s        2.000     -0.251599       -6.84637  3.269

Energies:          [Hartree]           [eV]
--------------------------------------------
 kinetic         +199.296038    +5423.12142
 coulomb (e-e)    +95.615592    +2601.83279
 coulomb (e-n)   -478.479101   -13020.07952
 xc               -15.967242     -434.49077
 total           -199.534712    -5429.61609

Exact exchange energy: -15.967256 Hartree, -434.49116 eV
Auxiliary basis Hartree Energy    +95.615592
Fraction     +1.000000



 ITERATION  ENERGY          1e-ENERGY        2e-ENERGY     NORM[dD(SAO)]  TOL
   7  -199.53471767428    -279.18306821     79.648350535    0.151D-14 0.121D-14
                           RI-K = -15.9672414555     Coul =  95.6155919906
                            current damping = 0.100

          Norm of current diis error: 0.40548E-11

 End of SCF iterations

   convergence criteria satisfied after     7 iterations


                  ------------------------------------------
                 |  total energy      =   -199.53471767428  |
                  ------------------------------------------
                 :  kinetic energy    =    199.29603768549  :
                 :  potential energy  =   -398.83075535978  :
                 :  virial theorem    =      1.99880381724  :
                 :  wavefunction norm =      1.00000000000  :
                  ..........................................


 <geterg> : there is no data group $energy


 <skperg> : $end is missing


 orbitals $scfmo  will be written to file mos

    irrep                  1a1g        2a1g        3a1g        4a1g
 eigenvalues H        -49.03871    -3.77672    -0.25160     0.13503
            eV       -1334.4217   -102.7706     -6.8464      3.6744
 occupation              2.0000      2.0000      2.0000

    irrep                  1t1u        2t1u
 eigenvalues H         -2.29205     0.24835
            eV         -62.3704      6.7581
 occupation          3 * 2.0000


"""


"""

                  ------------------------------------------
                 |  total energy      =   -199.53472370545  |
                  ------------------------------------------
                 :  kinetic energy    =    199.29604777288  :
                 :  potential energy  =   -398.83077147834  :
                 :  virial theorem    =      1.99880383761  :
                 :  wavefunction norm =      1.00000000000  :
                  ..........................................


 <geterg> : there is no data group $energy


 <skperg> : $end is missing


 orbitals $scfmo  will be written to file mos

    irrep                  1a1g        2a1g        3a1g        4a1g
 eigenvalues H        -49.03871    -3.77672    -0.25160     0.13503
            eV       -1334.4216   -102.7706     -6.8464      3.6745
 occupation              2.0000      2.0000      2.0000

    irrep                  1t1u        2t1u
 eigenvalues H         -2.29205     0.24835
            eV         -62.3704      6.7581
 occupation          3 * 2.0000


"""




"""

RI-JK-DFT

 ITERATION  ENERGY          1e-ENERGY        2e-ENERGY     NORM[dD(SAO)]  TOL
   9  -199.53471767428    -279.18306897     79.648351295    0.708D-03 0.951D-10
                           RI-K = -15.9672414902     Coul =  95.6155927849
                            current damping = 0.100

          Norm of current diis error: 0.99876E-07

 End of SCF iterations

   convergence criteria satisfied after     9 iterations


                  ------------------------------------------
                 |  total energy      =   -199.53471767428  |
                  ------------------------------------------
                 :  kinetic energy    =    199.29603782408  :
                 :  potential energy  =   -398.83075549836  :
                 :  virial theorem    =      1.99880381794  :
                 :  wavefunction norm =      1.00000000000  :
                  ..........................................


 <geterg> : there is no data group $energy


 <skperg> : $end is missing


 orbitals $scfmo  will be written to file mos

    irrep                  1a1g        2a1g        3a1g        4a1g
 eigenvalues H        -49.03871    -3.77672    -0.25160     0.13503
            eV       -1334.4217   -102.7706     -6.8464      3.6744
 occupation              2.0000      2.0000      2.0000

    irrep                  1t1u        2t1u
 eigenvalues H         -2.29205     0.24835
            eV         -62.3704      6.7581
 occupation          3 * 2.0000




 ==============================================================================
                           electrostatic moments
 ==============================================================================

 reference point for electrostatic moments:    0.00000   0.00000   0.00000

"""
